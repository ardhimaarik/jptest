package project.arrik.com.jobplanet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erdearik on 11/26/16.
 */

public class ResponseModel {
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("minimum_reviews")
    @Expose
    private Integer minimumReviews;
    @SerializedName("minimum_salaries")
    @Expose
    private Integer minimumSalaries;
    @SerializedName("minimum_interviews")
    @Expose
    private Integer minimumInterviews;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();

    /**
     *
     * @return
     * The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     *
     * @return
     * The totalCount
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     * The total_count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The page_size
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The totalPage
     */
    public Integer getTotalPage() {
        return totalPage;
    }

    /**
     *
     * @param totalPage
     * The total_page
     */
    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    /**
     *
     * @return
     * The minimumReviews
     */
    public Integer getMinimumReviews() {
        return minimumReviews;
    }

    /**
     *
     * @param minimumReviews
     * The minimum_reviews
     */
    public void setMinimumReviews(Integer minimumReviews) {
        this.minimumReviews = minimumReviews;
    }

    /**
     *
     * @return
     * The minimumSalaries
     */
    public Integer getMinimumSalaries() {
        return minimumSalaries;
    }

    /**
     *
     * @param minimumSalaries
     * The minimum_salaries
     */
    public void setMinimumSalaries(Integer minimumSalaries) {
        this.minimumSalaries = minimumSalaries;
    }

    /**
     *
     * @return
     * The minimumInterviews
     */
    public Integer getMinimumInterviews() {
        return minimumInterviews;
    }

    /**
     *
     * @param minimumInterviews
     * The minimum_interviews
     */
    public void setMinimumInterviews(Integer minimumInterviews) {
        this.minimumInterviews = minimumInterviews;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }
}
