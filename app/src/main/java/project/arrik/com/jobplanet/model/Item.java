package project.arrik.com.jobplanet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erdearik on 11/26/16.
 */

public class Item {
    @SerializedName("cell_type")
    @Expose
    private String cellType;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo_path")
    @Expose
    private String logoPath;
    @SerializedName("web_site")
    @Expose
    private Object webSite;
    @SerializedName("industry_id")
    @Expose
    private Integer industryId;
    @SerializedName("industry_name")
    @Expose
    private String industryName;
    @SerializedName("simple_desc")
    @Expose
    private String simpleDesc;
    @SerializedName("rate_total_avg")
    @Expose
    private Double rateTotalAvg;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("review_summary")
    @Expose
    private String reviewSummary;
    @SerializedName("salary_avg")
    @Expose
    private Integer salaryAvg;
    @SerializedName("interview_question")
    @Expose
    private String interviewQuestion;
    @SerializedName("interview_difficulty")
    @Expose
    private Double interviewDifficulty;
    @SerializedName("has_job_posting")
    @Expose
    private Boolean hasJobPosting;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("themes")
    @Expose
    private List<Theme> themes = new ArrayList<Theme>();
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("label_id")
    @Expose
    private Object labelId;
    @SerializedName("city_ids")
    @Expose
    private List<Integer> cityIds = new ArrayList<Integer>();
    @SerializedName("occupation_ids")
    @Expose
    private List<Integer> occupationIds = new ArrayList<Integer>();
    @SerializedName("recruitment_type_ids")
    @Expose
    private List<Integer> recruitmentTypeIds = new ArrayList<Integer>();
    @SerializedName("deadline")
    @Expose
    private Deadline deadline;
    @SerializedName("is_interview")
    @Expose
    private Boolean isInterview;
    @SerializedName("job_type_ids")
    @Expose
    private List<Integer> jobTypeIds = new ArrayList<Integer>();
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("review_avg_cache")
    @Expose
    private Double reviewAvgCache;
    @SerializedName("is_saved")
    @Expose
    private Boolean isSaved;
    @SerializedName("occupation_name")
    @Expose
    private String occupationName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("days_ago")
    @Expose
    private Integer daysAgo;
    @SerializedName("pros")
    @Expose
    private String pros;
    @SerializedName("cons")
    @Expose
    private String cons;
    @SerializedName("signed_in")
    @Expose
    private Integer signedIn;
    @SerializedName("hide_detail")
    @Expose
    private Integer hideDetail;
    @SerializedName("salary_ranking")
    @Expose
    private String salaryRanking;
    @SerializedName("salary_highest")
    @Expose
    private Integer salaryHighest;
    @SerializedName("salary_lowest")
    @Expose
    private Integer salaryLowest;
    @SerializedName("interview_summary")
    @Expose
    private String interviewSummary;
    @SerializedName("interview_range_start")
    @Expose
    private Double interviewRangeStart;
    @SerializedName("interview_range_end")
    @Expose
    private Double interviewRangeEnd;
    @SerializedName("interview_desc")
    @Expose
    private String interviewDesc;
    @SerializedName("interview_answer")
    @Expose
    private String interviewAnswer;
    @SerializedName("smb")
    @Expose
    private List<Object> smb = new ArrayList<Object>();

    /**
     *
     * @return
     * The cellType
     */
    public String getCellType() {
        return cellType;
    }

    /**
     *
     * @param cellType
     * The cell_type
     */
    public void setCellType(String cellType) {
        this.cellType = cellType;
    }

    /**
     *
     * @return
     * The companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     *
     * @param companyId
     * The company_id
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The logoPath
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     *
     * @param logoPath
     * The logo_path
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    /**
     *
     * @return
     * The webSite
     */
    public Object getWebSite() {
        return webSite;
    }

    /**
     *
     * @param webSite
     * The web_site
     */
    public void setWebSite(Object webSite) {
        this.webSite = webSite;
    }

    /**
     *
     * @return
     * The industryId
     */
    public Integer getIndustryId() {
        return industryId;
    }

    /**
     *
     * @param industryId
     * The industry_id
     */
    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    /**
     *
     * @return
     * The industryName
     */
    public String getIndustryName() {
        return industryName;
    }

    /**
     *
     * @param industryName
     * The industry_name
     */
    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    /**
     *
     * @return
     * The simpleDesc
     */
    public String getSimpleDesc() {
        return simpleDesc;
    }

    /**
     *
     * @param simpleDesc
     * The simple_desc
     */
    public void setSimpleDesc(String simpleDesc) {
        this.simpleDesc = simpleDesc;
    }

    /**
     *
     * @return
     * The rateTotalAvg
     */
    public Double getRateTotalAvg() {
        return rateTotalAvg;
    }

    /**
     *
     * @param rateTotalAvg
     * The rate_total_avg
     */
    public void setRateTotalAvg(Double rateTotalAvg) {
        this.rateTotalAvg = rateTotalAvg;
    }

    /**
     *
     * @return
     * The ranking
     */
    public Integer getRanking() {
        return ranking;
    }

    /**
     *
     * @param ranking
     * The ranking
     */
    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    /**
     *
     * @return
     * The reviewSummary
     */
    public String getReviewSummary() {
        return reviewSummary;
    }

    /**
     *
     * @param reviewSummary
     * The review_summary
     */
    public void setReviewSummary(String reviewSummary) {
        this.reviewSummary = reviewSummary;
    }

    /**
     *
     * @return
     * The salaryAvg
     */
    public Integer getSalaryAvg() {
        return salaryAvg;
    }

    /**
     *
     * @param salaryAvg
     * The salary_avg
     */
    public void setSalaryAvg(Integer salaryAvg) {
        this.salaryAvg = salaryAvg;
    }

    /**
     *
     * @return
     * The interviewQuestion
     */
    public String getInterviewQuestion() {
        return interviewQuestion;
    }

    /**
     *
     * @param interviewQuestion
     * The interview_question
     */
    public void setInterviewQuestion(String interviewQuestion) {
        this.interviewQuestion = interviewQuestion;
    }

    /**
     *
     * @return
     * The interviewDifficulty
     */
    public Double getInterviewDifficulty() {
        return interviewDifficulty;
    }

    /**
     *
     * @param interviewDifficulty
     * The interview_difficulty
     */
    public void setInterviewDifficulty(Double interviewDifficulty) {
        this.interviewDifficulty = interviewDifficulty;
    }

    /**
     *
     * @return
     * The hasJobPosting
     */
    public Boolean getHasJobPosting() {
        return hasJobPosting;
    }

    /**
     *
     * @param hasJobPosting
     * The has_job_posting
     */
    public void setHasJobPosting(Boolean hasJobPosting) {
        this.hasJobPosting = hasJobPosting;
    }

    /**
     *
     * @return
     * The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     *
     * @param count
     * The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     *
     * @return
     * The themes
     */
    public List<Theme> getThemes() {
        return themes;
    }

    /**
     *
     * @param themes
     * The themes
     */
    public void setThemes(List<Theme> themes) {
        this.themes = themes;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The labelId
     */
    public Object getLabelId() {
        return labelId;
    }

    /**
     *
     * @param labelId
     * The label_id
     */
    public void setLabelId(Object labelId) {
        this.labelId = labelId;
    }

    /**
     *
     * @return
     * The cityIds
     */
    public List<Integer> getCityIds() {
        return cityIds;
    }

    /**
     *
     * @param cityIds
     * The city_ids
     */
    public void setCityIds(List<Integer> cityIds) {
        this.cityIds = cityIds;
    }

    /**
     *
     * @return
     * The occupationIds
     */
    public List<Integer> getOccupationIds() {
        return occupationIds;
    }

    /**
     *
     * @param occupationIds
     * The occupation_ids
     */
    public void setOccupationIds(List<Integer> occupationIds) {
        this.occupationIds = occupationIds;
    }

    /**
     *
     * @return
     * The recruitmentTypeIds
     */
    public List<Integer> getRecruitmentTypeIds() {
        return recruitmentTypeIds;
    }

    /**
     *
     * @param recruitmentTypeIds
     * The recruitment_type_ids
     */
    public void setRecruitmentTypeIds(List<Integer> recruitmentTypeIds) {
        this.recruitmentTypeIds = recruitmentTypeIds;
    }

    /**
     *
     * @return
     * The deadline
     */
    public Deadline getDeadline() {
        return deadline;
    }

    /**
     *
     * @param deadline
     * The deadline
     */
    public void setDeadline(Deadline deadline) {
        this.deadline = deadline;
    }

    /**
     *
     * @return
     * The isInterview
     */
    public Boolean getIsInterview() {
        return isInterview;
    }

    /**
     *
     * @param isInterview
     * The is_interview
     */
    public void setIsInterview(Boolean isInterview) {
        this.isInterview = isInterview;
    }

    /**
     *
     * @return
     * The jobTypeIds
     */
    public List<Integer> getJobTypeIds() {
        return jobTypeIds;
    }

    /**
     *
     * @param jobTypeIds
     * The job_type_ids
     */
    public void setJobTypeIds(List<Integer> jobTypeIds) {
        this.jobTypeIds = jobTypeIds;
    }

    /**
     *
     * @return
     * The companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     *
     * @param companyName
     * The company_name
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     *
     * @return
     * The reviewAvgCache
     */
    public Double getReviewAvgCache() {
        return reviewAvgCache;
    }

    /**
     *
     * @param reviewAvgCache
     * The review_avg_cache
     */
    public void setReviewAvgCache(Double reviewAvgCache) {
        this.reviewAvgCache = reviewAvgCache;
    }

    /**
     *
     * @return
     * The isSaved
     */
    public Boolean getIsSaved() {
        return isSaved;
    }

    /**
     *
     * @param isSaved
     * The is_saved
     */
    public void setIsSaved(Boolean isSaved) {
        this.isSaved = isSaved;
    }

    /**
     *
     * @return
     * The occupationName
     */
    public String getOccupationName() {
        return occupationName;
    }

    /**
     *
     * @param occupationName
     * The occupation_name
     */
    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The daysAgo
     */
    public Integer getDaysAgo() {
        return daysAgo;
    }

    /**
     *
     * @param daysAgo
     * The days_ago
     */
    public void setDaysAgo(Integer daysAgo) {
        this.daysAgo = daysAgo;
    }

    /**
     *
     * @return
     * The pros
     */
    public String getPros() {
        return pros;
    }

    /**
     *
     * @param pros
     * The pros
     */
    public void setPros(String pros) {
        this.pros = pros;
    }

    /**
     *
     * @return
     * The cons
     */
    public String getCons() {
        return cons;
    }

    /**
     *
     * @param cons
     * The cons
     */
    public void setCons(String cons) {
        this.cons = cons;
    }

    /**
     *
     * @return
     * The signedIn
     */
    public Integer getSignedIn() {
        return signedIn;
    }

    /**
     *
     * @param signedIn
     * The signed_in
     */
    public void setSignedIn(Integer signedIn) {
        this.signedIn = signedIn;
    }

    /**
     *
     * @return
     * The hideDetail
     */
    public Integer getHideDetail() {
        return hideDetail;
    }

    /**
     *
     * @param hideDetail
     * The hide_detail
     */
    public void setHideDetail(Integer hideDetail) {
        this.hideDetail = hideDetail;
    }

    /**
     *
     * @return
     * The salaryRanking
     */
    public String getSalaryRanking() {
        return salaryRanking;
    }

    /**
     *
     * @param salaryRanking
     * The salary_ranking
     */
    public void setSalaryRanking(String salaryRanking) {
        this.salaryRanking = salaryRanking;
    }

    /**
     *
     * @return
     * The salaryHighest
     */
    public Integer getSalaryHighest() {
        return salaryHighest;
    }

    /**
     *
     * @param salaryHighest
     * The salary_highest
     */
    public void setSalaryHighest(Integer salaryHighest) {
        this.salaryHighest = salaryHighest;
    }

    /**
     *
     * @return
     * The salaryLowest
     */
    public Integer getSalaryLowest() {
        return salaryLowest;
    }

    /**
     *
     * @param salaryLowest
     * The salary_lowest
     */
    public void setSalaryLowest(Integer salaryLowest) {
        this.salaryLowest = salaryLowest;
    }

    /**
     *
     * @return
     * The interviewSummary
     */
    public String getInterviewSummary() {
        return interviewSummary;
    }

    /**
     *
     * @param interviewSummary
     * The interview_summary
     */
    public void setInterviewSummary(String interviewSummary) {
        this.interviewSummary = interviewSummary;
    }

    /**
     *
     * @return
     * The interviewRangeStart
     */
    public Double getInterviewRangeStart() {
        return interviewRangeStart;
    }

    /**
     *
     * @param interviewRangeStart
     * The interview_range_start
     */
    public void setInterviewRangeStart(Double interviewRangeStart) {
        this.interviewRangeStart = interviewRangeStart;
    }

    /**
     *
     * @return
     * The interviewRangeEnd
     */
    public Double getInterviewRangeEnd() {
        return interviewRangeEnd;
    }

    /**
     *
     * @param interviewRangeEnd
     * The interview_range_end
     */
    public void setInterviewRangeEnd(Double interviewRangeEnd) {
        this.interviewRangeEnd = interviewRangeEnd;
    }

    /**
     *
     * @return
     * The interviewDesc
     */
    public String getInterviewDesc() {
        return interviewDesc;
    }

    /**
     *
     * @param interviewDesc
     * The interview_desc
     */
    public void setInterviewDesc(String interviewDesc) {
        this.interviewDesc = interviewDesc;
    }

    /**
     *
     * @return
     * The interviewAnswer
     */
    public String getInterviewAnswer() {
        return interviewAnswer;
    }

    /**
     *
     * @param interviewAnswer
     * The interview_answer
     */
    public void setInterviewAnswer(String interviewAnswer) {
        this.interviewAnswer = interviewAnswer;
    }

    /**
     *
     * @return
     * The smb
     */
    public List<Object> getSmb() {
        return smb;
    }

    /**
     *
     * @param smb
     * The smb
     */
    public void setSmb(List<Object> smb) {
        this.smb = smb;
    }
}
