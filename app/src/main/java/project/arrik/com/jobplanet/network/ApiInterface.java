package project.arrik.com.jobplanet.network;

import project.arrik.com.jobplanet.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by erdearik on 11/27/16.
 */

public interface ApiInterface {
    @GET("/jp_main.json")
    Call<ResponseModel> getData();
}
