package project.arrik.com.jobplanet.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.arrik.com.jobplanet.R;
import project.arrik.com.jobplanet.model.Theme;

/**
 * Created by erdearik on 11/27/16.
 */

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
    private Context context;
    private List<Theme> charts;

    public HorizontalAdapter(Context context, List<Theme> charts) {
        this.context = context;
        this.charts = charts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_horizontal_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Theme item = charts.get(position);
        Glide.with(context).load(item.getCoverImage()).into(holder.icImage);
        holder.tvTitle.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return charts.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.ic_image)
        ImageView icImage;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
