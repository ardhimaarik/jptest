package project.arrik.com.jobplanet.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import project.arrik.com.jobplanet.R;
import project.arrik.com.jobplanet.model.Item;

/**
 * Created by erdearik on 11/27/16.
 */

public class ListAdapter extends RecyclerView.Adapter {
    private static final String TYPE_COMPANY = "CELL_TYPE_COMPANY";
    private static final String TYPE_HORIZONTAL_THEME = "CELL_TYPE_HORIZONTAL_THEME";
    private static final String TYPE_JOB = "CELL_TYPE_JOB_POSTING";
    private static final String TYPE_REVIEW = "CELL_TYPE_REVIEW";
    private static final String TYPE_SALARY = "CELL_TYPE_SALARY";
    private static final String TYPE_INTERVIEW = "CELL_TYPE_INTERVIEW";

    private static final int INT_TYPE_COMPANY = 1;
    private static final int INT_TYPE_HORIZONTAL_THEME = 2;
    private static final int INT_TYPE_JOB = 3;
    private static final int INT_TYPE_REVIEW = 4;
    private static final int INT_TYPE_SALARY = 5;
    private static final int INT_TYPE_INTERVIEW = 6;

    private List<Item> mList;
    private Context context;

    public ListAdapter(List<Item> mList, Context context) {
        this.mList = mList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case INT_TYPE_COMPANY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company, parent, false);
                return new CompanyHolder(view);
            case INT_TYPE_HORIZONTAL_THEME:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horizontal, parent, false);
                return new HorizontalHolder(view);
            case INT_TYPE_JOB:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job, parent, false);
                return new JobHolder(view);
            case INT_TYPE_REVIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false);
                return new ReviewHolder(view);
            case INT_TYPE_SALARY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_salary, parent, false);
                return new SalaryHolder(view);
            case INT_TYPE_INTERVIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interview, parent, false);
                return new InterviewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Item object = mList.get(position);
        if (object != null) {
            String type = object.getCellType();
            if (type.equals(TYPE_COMPANY)){
                Glide.with(context).load(object.getLogoPath()).into(((CompanyHolder) holder).ivLogo);
                ((CompanyHolder) holder).tvName.setText(object.getName());
                try {
                    ((CompanyHolder) holder).tvRating.setText(object.getRanking().toString());
                } catch (Exception e) {
                    ((CompanyHolder) holder).tvRating.setText("0");
                }
                ((CompanyHolder) holder).tvIndustriName.setText(object.getIndustryName());
                ((CompanyHolder) holder).tvReviewSummary.setText(object.getReviewSummary());
                ((CompanyHolder) holder).tvSalary.setText(object.getSalaryAvg().toString());
                ((CompanyHolder) holder).tvInterviewQuestion.setText(object.getInterviewSummary());
            }
            else if (type.equals(TYPE_HORIZONTAL_THEME)){
                LinearLayoutManager manager = new LinearLayoutManager(context);
                manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                ((HorizontalHolder) holder).rvList.setLayoutManager(manager);
                ((HorizontalHolder) holder).rvList.setHasFixedSize(true);
                ((HorizontalHolder) holder).rvList.setNestedScrollingEnabled(true);
                HorizontalAdapter adapter = new HorizontalAdapter(context,object.getThemes());
                ((HorizontalHolder) holder).rvList.setAdapter(adapter);
            }
            else if (type.equals(TYPE_INTERVIEW)){
                Glide.with(context).load(object.getLogoPath()).into(((InterviewHolder) holder).ivLogo);
                ((InterviewHolder) holder).tvName.setText(object.getName());
                try {
                    ((InterviewHolder) holder).tvRating.setText(object.getRanking().toString());
                } catch (Exception e) {
                    ((InterviewHolder) holder).tvRating.setText("0");
                }

                ((InterviewHolder) holder).tvIndustriName.setText(object.getIndustryName());
                ((InterviewHolder) holder).tvInterviewQuestion.setText(object.getInterviewQuestion());
            }
            else if (type.equals(TYPE_JOB)){
                Glide.with(context).load(object.getLogoPath()).into(((JobHolder) holder).ivLogo);
                ((JobHolder) holder).tvCompanyName.setText(object.getCompanyName());
                ((JobHolder) holder).tvTitle.setText(object.getTitle());
                ((JobHolder) holder).tvMessage.setText(object.getDeadline().getMessage());
                try {
                    ((JobHolder) holder).tvRating.setText(object.getRanking().toString());
                } catch (Exception e) {
                    ((JobHolder) holder).tvRating.setText("0");
                }

                ((JobHolder) holder).tvIndustriName.setText(object.getIndustryName());
            }
            else if (type.equals(TYPE_REVIEW)){
                Glide.with(context).load(object.getLogoPath()).into(((ReviewHolder) holder).ivLogo);
                ((ReviewHolder) holder).tvName.setText(object.getName());
                try {
                    ((ReviewHolder) holder).tvRating.setText(object.getRanking().toString());
                } catch (Exception e) {
                    ((ReviewHolder) holder).tvRating.setText("0");
                }
                ((ReviewHolder) holder).tvRating.setText(object.getRanking().toString());
                ((ReviewHolder) holder).tvIndustriName.setText(object.getIndustryName());
                ((ReviewHolder) holder).tvReviewSummary.setText(object.getReviewSummary());
                ((ReviewHolder) holder).tvPros.setText(object.getPros());
                ((ReviewHolder) holder).tvCons.setText(object.getCons());
            }
            else if (type.equals(TYPE_SALARY)){
                Glide.with(context).load(object.getLogoPath()).into(((SalaryHolder) holder).ivLogo);
                ((SalaryHolder) holder).tvName.setText(object.getName());
                try {
                    ((SalaryHolder) holder).tvRating.setText(object.getRanking().toString());
                } catch (Exception e) {
                    ((SalaryHolder) holder).tvRating.setText("0");
                }
                ((SalaryHolder) holder).tvIndustriName.setText(object.getIndustryName());
                try {
                    ((SalaryHolder) holder).tvSalary.setText(object.getSalaryAvg().toString());
                } catch (Exception e) {
                    ((SalaryHolder) holder).tvSalary.setText("0");
                }

                ((SalaryHolder) holder).tvMinSalari.setText(object.getSalaryLowest().toString());
                ((SalaryHolder) holder).tvMaxSalari.setText(object.getSalaryHighest().toString());
                ((SalaryHolder) holder).sbSalary.setMax(object.getSalaryLowest());
                ((SalaryHolder) holder).sbSalary.setMax(object.getSalaryHighest());
                ((SalaryHolder) holder).sbSalary.setProgress(object.getSalaryAvg());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null) {
            Item object = mList.get(position);
            if (object != null) {
                String type = object.getCellType();
                if (type.equals(TYPE_COMPANY)) return INT_TYPE_COMPANY;
                else if (type.equals(TYPE_HORIZONTAL_THEME)) return INT_TYPE_HORIZONTAL_THEME;
                else if (type.equals(TYPE_INTERVIEW)) return INT_TYPE_INTERVIEW;
                else if (type.equals(TYPE_JOB)) return INT_TYPE_JOB;
                else if (type.equals(TYPE_REVIEW)) return INT_TYPE_REVIEW;
                else if (type.equals(TYPE_SALARY)) return INT_TYPE_SALARY;
            }
        }
        return 0;
    }

    public static class CompanyHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.iv_logo)
        ImageView ivLogo;
//        @BindView(R.id.tv_name)
        TextView tvName;
//        @BindView(R.id.tv_rating)
        TextView tvRating;
//        @BindView(R.id.tv_industri_name)
        TextView tvIndustriName;
//        @BindView(R.id.tv_review_summary)
        TextView tvReviewSummary;
//        @BindView(R.id.tv_salary)
        TextView tvSalary;
//        @BindView(R.id.tv_interview_question)
        TextView tvInterviewQuestion;

        public CompanyHolder(View itemView) {
            super(itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvIndustriName = (TextView) itemView.findViewById(R.id.tv_industri_name);
            tvReviewSummary = (TextView) itemView.findViewById(R.id.tv_review_summary);
            tvSalary = (TextView) itemView.findViewById(R.id.tv_salary);
            tvInterviewQuestion = (TextView) itemView.findViewById(R.id.tv_interview_question);
//            ButterKnife.bind(this, itemView);
        }
    }

    public static class HorizontalHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.rv_list)
        RecyclerView rvList;

        public HorizontalHolder(View itemView) {
            super(itemView);
            rvList = (RecyclerView) itemView.findViewById(R.id.rv_list);
//            ButterKnife.bind(this, itemView);
        }
    }

    public static class JobHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.iv_logo)
        ImageView ivLogo;
//        @BindView(R.id.tv_company_name)
        TextView tvCompanyName;
//        @BindView(R.id.tv_title)
        TextView tvTitle;
//        @BindView(R.id.tv_message)
        TextView tvMessage;
//        @BindView(R.id.tv_rating)
        TextView tvRating;
//        @BindView(R.id.tv_industri_name)
        TextView tvIndustriName;

        public JobHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tvCompanyName = (TextView) itemView.findViewById(R.id.tv_company_name);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvIndustriName = (TextView) itemView.findViewById(R.id.tv_industri_name);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
        }
    }

    public static class ReviewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.iv_logo)
        ImageView ivLogo;
//        @BindView(R.id.tv_name)
        TextView tvName;
//        @BindView(R.id.tv_rating)
        TextView tvRating;
//        @BindView(R.id.tv_industri_name)
        TextView tvIndustriName;
//        @BindView(R.id.tv_review_summary)
        TextView tvReviewSummary;
//        @BindView(R.id.tv_pros)
        TextView tvPros;
//        @BindView(R.id.tv_cons)
        TextView tvCons;

        public ReviewHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvIndustriName = (TextView) itemView.findViewById(R.id.tv_industri_name);
            tvReviewSummary = (TextView) itemView.findViewById(R.id.tv_review_summary);
            tvPros = (TextView) itemView.findViewById(R.id.tv_pros);
            tvCons = (TextView) itemView.findViewById(R.id.tv_cons);
        }
    }

    public static class SalaryHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.iv_logo)
        ImageView ivLogo;
//        @BindView(R.id.tv_name)
        TextView tvName;
//        @BindView(R.id.tv_rating)
        TextView tvRating;
//        @BindView(R.id.tv_industri_name)
        TextView tvIndustriName;
//        @BindView(R.id.tv_salary)
        TextView tvSalary;
//        @BindView(R.id.sb_salary)
        SeekBar sbSalary;
//        @BindView(R.id.tv_min_salari)
        TextView tvMinSalari;
//        @BindView(R.id.tv_max_salari)
        TextView tvMaxSalari;

        public SalaryHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvIndustriName = (TextView) itemView.findViewById(R.id.tv_industri_name);
            tvSalary = (TextView) itemView.findViewById(R.id.tv_salary);
            tvMinSalari = (TextView) itemView.findViewById(R.id.tv_min_salari);
            tvMaxSalari = (TextView) itemView.findViewById(R.id.tv_max_salari);
            sbSalary = (SeekBar) itemView.findViewById(R.id.sb_salary);
        }
    }

    public static class InterviewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.iv_logo)
        ImageView ivLogo;
//        @BindView(R.id.tv_name)
        TextView tvName;
//        @BindView(R.id.tv_rating)
        TextView tvRating;
//        @BindView(R.id.tv_industri_name)
        TextView tvIndustriName;
//        @BindView(R.id.tv_interview_question)
        TextView tvInterviewQuestion;

        public InterviewHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            ivLogo = (ImageView) itemView.findViewById(R.id.iv_logo);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvIndustriName = (TextView) itemView.findViewById(R.id.tv_industri_name);
            tvInterviewQuestion = (TextView) itemView.findViewById(R.id.tv_interview_question);
        }
    }
}
