package project.arrik.com.jobplanet.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by erdearik on 11/27/16.
 */

public class ApiClient {
    public static final String BASE_URL = "https://hwang.run/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(MyJsonConverter.create())
                    .build();
        }
        return retrofit;
    }
}
