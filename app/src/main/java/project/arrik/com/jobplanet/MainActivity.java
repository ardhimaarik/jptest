package project.arrik.com.jobplanet;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.arrik.com.jobplanet.adapter.ListAdapter;
import project.arrik.com.jobplanet.model.Item;
import project.arrik.com.jobplanet.model.ResponseModel;
import project.arrik.com.jobplanet.network.ApiClient;
import project.arrik.com.jobplanet.network.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    private ProgressDialog mProgressDialog;
    private ListAdapter adapter;
    private List<Item> dataList;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        context = this;
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setHasFixedSize(true);

        getDataList();

    }

    private void getDataList() {
        showProgressDialog("please wait...");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseModel> call = apiService.getData();
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                dataList = response.body().getItems();
                adapter = new ListAdapter(dataList, context);
                rvList.setAdapter(adapter);
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Data Load Error "+t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.e("GetDataError",t.getMessage().toString());
                dismissProgressDialog();
            }
        });
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
