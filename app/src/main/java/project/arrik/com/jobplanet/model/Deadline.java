package project.arrik.com.jobplanet.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by erdearik on 11/26/16.
 */

public class Deadline {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("hex_color")
    @Expose
    private String hexColor;

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The hexColor
     */
    public String getHexColor() {
        return hexColor;
    }

    /**
     *
     * @param hexColor
     * The hex_color
     */
    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }
}
